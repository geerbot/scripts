# sed delete everything preceeding a pattern
echo "removemeHello World" | sed 's/^.*\(Hello\)/\1/'

# sed delete everything preceeding and including a pattern
echo "removemeHello World" | sed -n 's/^.*Hello//p'

# sed delete all preceeding directories and print the file
echo "./remove/this/directory/path/Hello/World" | sed -e 's`\([^/]*/\)*``'
